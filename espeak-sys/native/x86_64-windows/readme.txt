# eSpeak Win 64 shared lib

Shared library for eSpeak espeak_lib.dll compiled from source (1) with modifications from (2) with MS Visual Studio Community 2015 (3) as
release build without debug infos for x64.

(1) https://sourceforge.net/projects/espeak/files/espeak/espeak-1.48/espeak-1.48.04-source.zip
(2) msvc.x64.lib.patch
(3) https://www.visualstudio.com/
