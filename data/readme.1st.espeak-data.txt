Required data for eSpeak has to be downloaded from the espeak src package:

- create a subfolder "espeak-data" in the data folder

- download http://sourceforge.net/projects/espeak/files/espeak/espeak-1.48/espeak-1.48.04-source.zip

- unpack the complete content of the espeak-data folder into the local espeak-data folder

- the espeak.<language-code>-custom.dict is an additional dictionary for user
  defined mappings (see comments in file) and is not part of the espeak
  distributions
