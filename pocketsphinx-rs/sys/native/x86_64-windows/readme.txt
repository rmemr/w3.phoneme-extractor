# pocketsphinx Win 64 precompiled lib for static linking

Pocketsphinx compiled from source (1) without modifications with MS Visual Studio Community 2022 (2)
as release build without debug infos for x64.

(1) https://github.com/cmusphinx/pocketsphinx (rev 1f146934e0735ca1e65143f959f670c67c5ac303, tag v5.0.3)
(2) https://www.visualstudio.com/
